<?php
return array(
    'title' => 'Миграции данных',
    'changeversion' => 'Версии платформ',
    'import' => 'Импорт миграций',
    'export' => 'Экспорт миграций',
    'apply' => 'Применение',
    'log' => 'Журнал изменений',
    'entitiesVersions' => 'Версии данных',
    'createScenario' => 'Создать сценарий обновления',
    'diagnostic' => 'Диагностика'
);